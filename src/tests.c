#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>


#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include "util.h"


// print title of the test
static void show_title(size_t test_num, char* title) {
    printf("\n-----===== Running test %zu: %s! =====-----\n", test_num, title);
}

// print test case
static void show_case(char* msg) {
    printf("\n=== Test case: %s ===\n", msg);
}

// print heap dump
static void show_dump(char* msg, void* heap) {
    printf("%s: \n", msg);
    debug_heap(stdout, heap);
}

// print complete message
static void show_complete(size_t test_num) {
    printf("\n-----===== Test %zu completed! =====-----\n", test_num);
}


// get header from heap
static struct block_header* get_block(void* heap, size_t num) {
    struct block_header* header = heap;
    size_t counter = 1;
    while (counter < num) {
        counter++;
        header = header->next;
    }
    return header;
}

static size_t count_blocks(void* heap) {
    size_t counter = 0;
    struct block_header* header = heap;
    while (header != NULL) {
        counter++;
        header = header->next;
    }
    return counter;
}

// ====================================== TESTS ==============================
// memory mapping
void test1() {
    show_title(1, "Memory mapping and allocation");

    show_case("Try to map 1000 bytes");
    void* heap = heap_init(SMALL_HEAP_SIZE);
    show_dump("Result", heap);
    if (get_block(heap, 1)->capacity.bytes != PAGE_SIZE * 2 - offsetof( struct block_header, contents )) {
        err("Error: capacity should be 8175!\n");
    }
    munmap(heap, PAGE_SIZE * 2);

    show_case("Try to map 10000 bytes");
    heap = heap_init(LARGE_HEAP_SIZE);
    show_dump("Result", heap);
    if (get_block(heap, 1)->capacity.bytes != PAGE_SIZE * 3 - offsetof( struct block_header, contents )) {
        err("Error: capacity should be 12271!\n");
    }

    show_case("Try to allocate 1000 bytes");
    _malloc(MEDUIM_BLOCK_SIZE);
    show_dump("Result", heap);

    if (get_block(heap, 1)->is_free != false || get_block(heap, 1)->capacity.bytes != MEDUIM_BLOCK_SIZE || count_blocks(heap) != 2) {
        err("Error: block should be taken and capacity of first block should be 1000!\n");
    }
    munmap(heap, PAGE_SIZE * 3);

    show_complete(1);
}

// memory freeing
void test2() {
    show_title(2, "Memory freeing");

    void* heap = heap_init(SMALL_HEAP_SIZE);
    void* first_block = _malloc(SMALL_BLOCK_SIZE);
    void* second_block = _malloc(SMALL_BLOCK_SIZE);
    void* third_block = _malloc(SMALL_BLOCK_SIZE);
    show_dump("Memory state before", heap);

    show_case("Try to free one block");
    _free(first_block);
    show_dump("Result", heap);
    if (get_block(heap, 1)->is_free == false || get_block(heap, 1)->capacity.bytes != SMALL_BLOCK_SIZE) {
        err("Error: block must be freed!\n");
    }

    show_case("Try free other blocks");
    _free(second_block);
    _free(third_block);
    show_dump("Result", heap);
    if (count_blocks(heap) != 3 || get_block(heap, 3)->is_free != true) {
        err("Error: last two blocks should merge and be free!\n");
    }

    show_case("Try to allocate 1000 bytes after that");
    _malloc(MEDUIM_BLOCK_SIZE);
    show_dump("Result", heap);

    if (get_block(heap, 1)->capacity.bytes != MEDUIM_BLOCK_SIZE || count_blocks(heap) != 2) {
        err("Error: Should be only 2 blocks!\n");
    }
    munmap(heap, PAGE_SIZE * 3);

    show_complete(2);
}

// additional page mapping
void test3() {
    show_title(3, "Additional page mapping");

    void* heap = heap_init(SMALL_HEAP_SIZE);
    _malloc(LARGE_BLOCK_SIZE);
    show_dump("Memory state before", heap);

    show_case("Try to allocate 8000 bytes");
    _malloc(LARGE_BLOCK_SIZE);
    show_dump("Result", heap);
    if (get_block(heap, 2)->is_free != false || get_block(heap, 2)->capacity.bytes != LARGE_BLOCK_SIZE || count_blocks(heap) != 3) {
        err("Error: pages must be allocated near the already allocated, so there will be only 3 blocks!\n");
    }

    munmap(heap, PAGE_SIZE * 5);

    show_complete(3);
}

// problemed additional page mapping
void test4() {
    show_title(4, "Problemed additional page mapping");

    void* heap = heap_init(SMALL_HEAP_SIZE);
    _malloc(LARGE_BLOCK_SIZE);
    show_dump("Memory state before", heap);

    show_case("Try to allocate 8000 bytes while bytes at 9000 atfer start of the heap mapped");
    mmap( (char*)HEAP_START + LARGE_BLOCK_SIZE + MEDUIM_BLOCK_SIZE, LARGE_BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
    _malloc(LARGE_BLOCK_SIZE);
    show_dump("Result", heap);
    if (get_block(heap, 3)->is_free != false || get_block(heap, 3)->capacity.bytes != LARGE_BLOCK_SIZE || count_blocks(heap) != 4) {
        err("Error: pages must be allocated somwhere else, so there will be 4 blocks!\n");
    }

    munmap(heap, PAGE_SIZE * 5);

    show_complete(4);
}

void run_tests() {
    test1();
    test2();
    test3();
    test4();
}