#ifndef _MEM_INTERNALS_
#define _MEM_INTERNALS_

#include <stddef.h>
#include <stdbool.h>
#include <inttypes.h>

// minimal additional bytes to map 
#define REGION_MIN_SIZE (2 * 4096)  

// region struct
struct region { void* addr; size_t size; bool extends; }; 
// stub for invalid region
static const struct region REGION_INVALID = {0}; 

// check if region is invalid
inline bool region_is_invalid( const struct region* r ) { return r->addr == NULL; } 

// useful space in block
typedef struct { size_t bytes; } block_capacity; 
// capacity + header in block
typedef struct { size_t bytes; } block_size; 

// header struct
struct block_header {
  struct block_header*    next; // pointer to the next block
  block_capacity capacity; // useful space in block
  bool           is_free; // can be used
  uint8_t        contents[]; // flexible array
};

// convert capacity to size
inline block_size size_from_capacity( block_capacity cap ) { return (block_size) {cap.bytes + offsetof( struct block_header, contents ) }; }
// convert size to capasity
inline block_capacity capacity_from_size( block_size sz ) { return (block_capacity) {sz.bytes - offsetof( struct block_header, contents ) }; }

#endif
