#include <stdio.h>
#include <stdint.h>

#include "tests.h"

#define SUCCESS 0;

int main(int argc, char const *argv[])
{
    run_tests();

    return SUCCESS;

    (void) argc; (void) argv;
}
