#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

// check if query fits the block
static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
// amount of maped pages
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
// size of all pages if they all are full
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

// init new block on given address of given size and pointer to next block
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

// size of region needed to allocate query
static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );


// map additional pages with flags
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( const void * addr, size_t query ) {
  /*  ??? */
  if (!addr) return REGION_INVALID;

  // calculate region params
  const size_t needed_size = query + offsetof( struct block_header, contents ); // we need to map (query + header size)
  const size_t region_size = region_actual_size(needed_size); 

  // map pages
  void* pages = map_pages(addr, region_size, MAP_FIXED_NOREPLACE); // try do it in this address
  if (pages == MAP_FAILED) {
    pages = map_pages(addr, region_size, 0); // try do it somewhere
  }
  if (pages == MAP_FAILED) return REGION_INVALID;

  // init block
  const block_size size = {region_size};
  block_init(pages, size, NULL);
  return (struct region) { .addr = pages, .size = region_size, .extends = addr == pages };
}

static void* block_after( struct block_header const* block )         ;

// try to init heap with first region
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

// minimal capacity of a block
#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
// check if block can be splitted and store the query
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

// try to split block to store query in first if it is too big
static bool split_if_too_big( struct block_header* block, size_t query ) {
  /*  ??? */
  assert(block); // NULL in block doesn't make sense

  if (block_splittable(block, query)) {
    // calculate new block params
    const block_size new_block_size = (block_size) { block->capacity.bytes - query };
    void* new_block_addr = block->contents + query;

    // init new block and copy pointer to next
    block_init(new_block_addr, new_block_size, block->next);

    // change old block params
    block->capacity.bytes = query;
    block->next = new_block_addr;

    return true;
  } 

  return false;
}


/*  --- Слияние соседних свободных блоков --- */
// get a pointer to the byte after block
static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
// check if the second block is right after the first block 
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

// check if two block can be merged
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

// try to merge block with the next one
static bool try_merge_with_next( struct block_header* block ) {
  /*  ??? */
  assert(block); // NULL in block doesn't make sense

  if (block->next && mergeable(block, block->next)) {
    const block_size next_block_size = size_from_capacity(block->next->capacity);

    // update block params
    block->capacity.bytes += next_block_size.bytes;
    block->next = block->next->next;

    return true;
  }

  return false;
}


/*  --- ... ecли размера кучи хватает --- */
// struct of a search result for a query allocation
struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

// try to find goog block for a query or return last
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  /*??? */
  if (!block) {
    return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
  }

  struct block_header* restrict prev = block;
  while (block) {
    // merge block with all free that next to him
    while (try_merge_with_next(block));

    if (block->is_free && block_is_big_enough(sz, block)) {
      return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block}; 
    }
    prev = block;
    block = block->next;
  }

  // return last block if didn't found good one
  return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = prev};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  /* ??? */
  assert(block); // NULL in block doesn't make sense

  const struct block_search_result result = find_good_or_last(block, query);

  // alloc if found good block
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }

  return result;
}

// try to map new pages for a query
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  /*  ??? */
  assert(last); // NULL in block doesn't make sense

  const struct region reg = alloc_region(block_after(last), query);
  if (region_is_invalid(&reg)) {
    return NULL;
  }
  last->next = reg.addr;

  return reg.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  /*  ??? */
  assert(heap_start);

  // choose capacity
  query = size_max(query, BLOCK_MIN_CAPACITY);

  struct block_search_result result = try_memalloc_existing(query, heap_start);

  if (result.type == BSR_REACHED_END_NOT_FOUND) {
    grow_heap(result.block, query);
    result = try_memalloc_existing(query, result.block);
  }

  if (result.type == BSR_CORRUPTED || result.type == BSR_REACHED_END_NOT_FOUND) {
    return NULL;
  }

  return result.block;
}

// make allocation trying from the start of the heap
void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

// get header of a block from a contents array
static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

// try to free block 
void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  /*  ??? */
  // try to merge with every next free block
  while(try_merge_with_next(header));
}
