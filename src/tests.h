#ifndef _TESTS_H_
#define _TESTS_H_


#define PAGE_SIZE 4096

#define SMALL_HEAP_SIZE 1000
#define LARGE_HEAP_SIZE 10000

#define SMALL_BLOCK_SIZE 100
#define MEDUIM_BLOCK_SIZE 1000
#define LARGE_BLOCK_SIZE 8000

#define MAP_ANONYMOUS (0x20)



void run_tests();

#endif
